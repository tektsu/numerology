#![warn(missing_docs)]

//! # date
//!
//! The date module preforms calculations on a valid date

use std::fmt;

use string_builder::Builder;

use crate::date::basic_date::BasicDate;
use crate::date::challenges::Challenges;
use crate::date::cycles::Cycles;
use crate::date::pinnacles::Pinnacles;
use crate::error::NumerolibError;
use crate::word::Word;

/// Implement the Path of Life
pub mod path_of_life;

/// Implement a basic date
pub mod basic_date;

/// Implement challenges
pub mod challenges;

/// Implement cycles
pub mod cycles;

/// Implement pinnacles
pub mod pinnacles;

/// A Date struct holds all the reductions of a valid date
pub struct Date {
    /// The basic date information for the given date
    pub basic: BasicDate,
    /// The Cycle results
    pub cycles: Cycles,
    /// The Challenge results
    pub challenges: Challenges,
    /// The Pinnacle results
    pub pinnacles: Pinnacles,
}

impl Date {
    /// Create a new ['Date'] instance
    ///
    /// # Arguments
    ///
    /// `date_in` - The date to be reduced; must be in the form "mm/dd/yyyy"
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::Date;
    /// let date = Date::new("1/24/1941").unwrap();
    /// assert_eq!(date.basic.year.word, "1941");
    /// assert_eq!(date.basic.year.result, 6);
    /// assert_eq!(date.basic.year.result_str, "6");
    /// assert_eq!(date.basic.month.word, "1");
    /// assert_eq!(date.basic.month.result, 1);
    /// assert_eq!(date.basic.month.result_str, "1");
    /// assert_eq!(date.basic.day.word, "24");
    /// assert_eq!(date.basic.day.result, 6);
    /// assert_eq!(date.basic.day.result_str, "6");
    /// assert_eq!(date.basic.path_of_life.result, 22);
    /// assert_eq!(date.basic.path_of_life.result_str, "22/4");
    /// ```
    ///
    pub fn new<S: Into<String>>(date_in: S) -> Result<Self, NumerolibError> {
        let basic = BasicDate::new(date_in)?;
        let cycles = Cycles::new(&basic);
        let challenges = Challenges::new(&basic);
        let pinnacles = Pinnacles::new(&basic);

        Ok(Self { basic, cycles, challenges, pinnacles })
    }

    /// Get a string representation of this Date
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::Date;
    /// let date_string = Date::new("9/24/1941").unwrap().to_string();
    /// ```
    ///
    pub fn to_string(&self) -> String {
        let mut builder = Builder::default();
        builder.append(self.basic.to_string());
        builder.append("\n");
        builder.append(self.cycles.to_string());
        builder.append("\n");
        builder.append(self.challenges.to_string());
        builder.append("\n");
        builder.append(self.pinnacles.to_string());
        builder.append("\n");

        builder.string().unwrap().trim().to_string()
    }
}

impl fmt::Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

/// Get the universal Year for a given year
///
/// # Arguments
///
/// `year` - The year for which to calculate Universal Year
///
/// # Examples
///
/// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 3, THE LIFE CYCLES OR SUB-PATHS
/// and Ch 10, THE PERSONAL VIBRATIONS
///
/// ```
/// use numerolib::date::*;
/// let uyear = universal_year(1973);
/// assert_eq!(uyear.result, 2);
/// ```
///
pub fn universal_year(year: u32) -> Word {
    Word::new(year.to_string())
}

/// Get the universal Year for a given year as a string (there is no check to ensure the string is a valid year)
///
/// # Arguments
///
/// `year` - The year for which to calculate Universal Year
///
/// # Examples
///
/// ```
/// use numerolib::date::*;
/// let uyear = universal_year_from_string("1996");
/// assert_eq!(uyear.result, 7);
/// ```
///
pub fn universal_year_from_string<S: Into<String>>(year: S) -> Word {
    Word::new(year.into())
}

//==================================================================================================
// Unit Tests
//==================================================================================================

#[cfg(test)]
mod tests {
    use crate::date::path_of_life::{negative, positive};

    use super::*;

    #[test]
    fn test_new_date() {
        match Date::new("9/31/1941") {
            Ok(_) => panic!("Test should not have passed"),
            Err(e) => {
                match e {
                    NumerolibError::InvalidDate(_) => {}
                    _ => panic!("Test should have failed with NumerolibError::InvalidDate()"),
                }
            }
        }
    }

    #[test]
    fn test_universal_year() {
        struct TestDef<'a> {
            year: u32,
            universal_year_str: &'a str,
            universal_year: u32,
        }

        let table = vec![
            TestDef { year: 1936, universal_year_str: "1", universal_year: 1 },
            TestDef { year: 1973, universal_year_str: "2", universal_year: 2 },
            TestDef { year: 1938, universal_year_str: "3", universal_year: 3 },
        ];

        for test in table {
            let run_tests = |word: Word, note: &str| {
                assert_eq!(word.result_str, test.universal_year_str, "year: [{}] note: [{}]", test.year, note);
                assert_eq!(word.result, test.universal_year, "year: [{}] note: [{}]", test.year, note);
            };
            run_tests(universal_year(test.year), "as u32");
            run_tests(universal_year_from_string(test.year.to_string()), "as string");
            run_tests(universal_year_from_string(test.year.to_string().as_str()), "as str");
        }
    }

    #[test]
    fn test_keywords() {
        struct TestDef<'a> {
            path_of_life: u32,
            positive: &'a str,
            negative: &'a str,
            err: bool,
        }

        let table = vec![
            TestDef { path_of_life: 0, positive: "Universal", negative: "Ideals", err: true },
            TestDef { path_of_life: 1, positive: "Individual", negative: "Self", err: false },
            TestDef { path_of_life: 11, positive: "Inspiration", negative: "Sacrifice", err: false },
            TestDef { path_of_life: 22, positive: "Universal", negative: "Ideals", err: false },
            TestDef { path_of_life: 23, positive: "Universal", negative: "Ideals", err: true },
        ];

        for test in table {
            let presult = positive::Positive::from_path_of_life(test.path_of_life);
            if test.err {
                match presult {
                    Ok(_) => panic!("This test should have returned an Err"),
                    Err(e) => {
                        match e {
                            NumerolibError::OutOfRangeError(i) => {
                                assert_eq!(i, test.path_of_life as usize);
                            }
                            _ => {
                                panic!("Should have returned a NumerolibError::OutOfRangeError()");
                            }
                        }
                    }
                }
            } else {
                match presult {
                    Ok(v) => assert_eq!(v.to_string(), test.positive),
                    Err(e) => panic!("{}", e),
                };
            }
            let nresult = negative::Negative::from_path_of_life(test.path_of_life);
            if test.err {
                match nresult {
                    Ok(_) => panic!("This test should have returned an Err"),
                    Err(_) => {}
                }
            } else {
                match nresult {
                    Ok(v) => assert_eq!(v.to_string(), test.negative),
                    Err(e) => panic!("{}", e),
                };
            }
        }
    }
}

