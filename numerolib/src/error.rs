use std::fmt::Debug;

/// NumerolibError defines the types of errors the library can throw
#[derive(Debug, thiserror::Error)]
pub enum NumerolibError {
    /// Error indicates that the given index does not exist
    #[error("Index is out of range")]
    OutOfRangeError(usize),

    /// Error indicates an age cannot be calculated as it would be negative
    #[error("Age would be negative")]
    AgeBeforeBirth(u32),

    /// Error indicates date is invalid or cannot be parsed
    #[error("Date is not valid")]
    InvalidDate(String),
}
