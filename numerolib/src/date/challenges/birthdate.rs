#![warn(missing_docs)]

//! # date:challenges::birthdate
//!
//! The date:challenges::birthdate module implements the birthdate challenges keywords

use std::fmt;

use crate::date::basic_date::BasicDate;
use crate::error::NumerolibError;

/// The Birthdate Challenges
#[allow(missing_docs)]
#[derive(Clone, Copy, Debug)]
pub enum Phrases {
    No(u32),
    Individualize(u32),
    Cooperate(u32),
    Energies(u32),
    ApplySelf(u32),
    Freedom(u32),
    Responsibilities(u32),
    SelfDiscipline(u32),
    Power(u32),
}

impl Phrases {
    /// Create a new phrase from a challenge
    ///
    /// # Arguments
    ///
    /// * `challenge` - The numeric challenge
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 5, CHALLENGE OF THE BIRTHDATE
    ///
    /// ```
    /// use numerolib::date::challenges::birthdate::Phrases;
    /// let phrase = Phrases::from_challenge(2).unwrap();
    /// assert_eq!(phrase.to_challenge(), 2);
    /// assert_eq!(phrase.to_string(), "To cooperate");
    /// ```
    ///
    /// ```
    /// use numerolib::date::challenges::birthdate::Phrases;
    /// let phrase = Phrases::from_challenge(5).unwrap();
    /// assert_eq!(phrase.to_challenge(), 5);
    /// assert_eq!(phrase.to_string(), "To accept change and use freedom correctly");
    /// ```
    ///
    pub fn from_challenge(challenge: u32) -> Result<Phrases, NumerolibError> {
        Ok(match challenge {
            0 => Phrases::No(challenge),
            1 => Phrases::Individualize(challenge),
            2 => Phrases::Cooperate(challenge),
            3 => Phrases::Energies(challenge),
            4 => Phrases::ApplySelf(challenge),
            5 => Phrases::Freedom(challenge),
            6 => Phrases::Responsibilities(challenge),
            7 => Phrases::SelfDiscipline(challenge),
            8 => Phrases::Power(challenge),
            _ => return Err(NumerolibError::OutOfRangeError(challenge as usize)),
        })
    }

    /// Convert to challenge
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::challenges::birthdate::Phrases;
    /// let phrase = Phrases::from_challenge(5).unwrap();
    /// assert_eq!(phrase.to_challenge(), 5);
    /// ```
    ///
    pub fn to_challenge(&self) -> u32 {
        match self {
            Phrases::No(challenge) |
            Phrases::Individualize(challenge) |
            Phrases::Cooperate(challenge) |
            Phrases::Energies(challenge) |
            Phrases::ApplySelf(challenge) |
            Phrases::Freedom(challenge) |
            Phrases::Responsibilities(challenge) |
            Phrases::SelfDiscipline(challenge) |
            Phrases::Power(challenge) => challenge.to_owned(),
        }
    }
}

impl fmt::Display for Phrases {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Phrases::No(_) => write!(f, "No Challenge"),
            Phrases::Individualize(_) => write!(f, "To individualize"),
            Phrases::Cooperate(_) => write!(f, "To cooperate"),
            Phrases::Energies(_) => write!(f, "To not scatter energies"),
            Phrases::ApplySelf(_) => write!(f, "To apply oneself to work"),
            Phrases::Freedom(_) => write!(f, "To accept change and use freedom correctly"),
            Phrases::Responsibilities(_) => write!(f, "To adjust to responsibilities"),
            Phrases::SelfDiscipline(_) => write!(f, "To obtain faith and self-discipline"),
            Phrases::Power(_) => write!(f, "To learn the correct use of money and power"),
        }
    }
}

/// Get the birthdate challenge from the BasicDate
///
/// # Arguments
///
/// * `date` - The date from which to calculate the challenge
///
/// # Examples
///
/// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 5, THE CHALLENGE OF THE BIRTHDATE
///
/// ```
/// use numerolib::date::challenges::birthdate;
/// use numerolib::date::basic_date::BasicDate;
/// let date = BasicDate::new("9/24/1941").unwrap();
/// match birthdate::challenge(&date) {
///     Some(phrase) => assert_eq!(phrase.to_challenge(), 2),
///     None => panic!("Challenge not set")
/// }
/// ```
///
/// ```
/// use numerolib::date::challenges::birthdate;
/// use numerolib::date::basic_date::BasicDate;
/// let date = BasicDate::new("9/22/1941").unwrap();
/// match birthdate::challenge(&date) {
///     Some(phrase) => assert_eq!(phrase.to_challenge(), 0),
///     None => panic!("Challenge not set")
/// }
/// ```
///
/// ```
/// use numerolib::date::challenges::birthdate;
/// use numerolib::date::basic_date::BasicDate;
/// let date = BasicDate::new("9/2/1941").unwrap();
/// match birthdate::challenge(&date) {
///     Some(_) => panic!("Unexpected challenge found"),
///     None => assert!(true),
/// }
/// ```
///
pub fn challenge(date: &BasicDate) -> Option<Phrases> {
    match date.day() {
        11 | 22 => Some(Phrases::from_challenge(0).unwrap()),
        10 | 12 | 21 | 23 => Some(Phrases::from_challenge(1).unwrap()),
        13 | 20 | 24 | 31 => Some(Phrases::from_challenge(2).unwrap()),
        14 | 25 | 30 => Some(Phrases::from_challenge(3).unwrap()),
        15 | 26 => Some(Phrases::from_challenge(4).unwrap()),
        16 | 27 => Some(Phrases::from_challenge(5).unwrap()),
        17 | 28 => Some(Phrases::from_challenge(6).unwrap()),
        18 | 29 => Some(Phrases::from_challenge(7).unwrap()),
        19 => Some(Phrases::from_challenge(8).unwrap()),
        _ => None,
    }
}



