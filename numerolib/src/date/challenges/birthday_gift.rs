#![warn(missing_docs)]

//! # date:challenges::birthday_gift
//!
//! The date:challenges::birthday_gift module implements the birthday gift keywords

use std::fmt;

use crate::date::challenges::birthdate::Phrases;
use crate::error::NumerolibError;

/// The Birthdate Gifts
#[allow(missing_docs)]
#[derive(Clone, Copy, Debug)]
pub enum Gifts {
    Attainment(u32),
    Intelligence(u32),
    Creativity(u32),
    Growth(u32),
    Stability(u32),
    Success(u32),
    Knowledge(u32),
    MaterialSuccess(u32),
}

impl Gifts {
    /// Create a new gift enum from a numeric gift
    ///
    /// # Arguments
    ///
    /// * `gift` - The numeric challenge
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 6, THE BIRTHDAY GIFT
    ///
    /// ```
    /// use numerolib::date::challenges::birthday_gift::Gifts;
    /// let gift = Gifts::from_gift(2).unwrap();
    /// assert_eq!(gift.to_gift(), 2);
    /// assert_eq!(gift.to_string(), "Intelligence");
    /// ```
    ///
    /// ```
    /// use numerolib::date::challenges::birthday_gift::Gifts;
    /// let gift = Gifts::from_gift(5).unwrap();
    /// assert_eq!(gift.to_gift(), 5);
    /// assert_eq!(gift.to_string(), "Stability");
    /// ```
    ///
    pub fn from_gift(gift: u32) -> Result<Gifts, NumerolibError> {
        Ok(match gift {
            1 => Gifts::Attainment(gift),
            2 => Gifts::Intelligence(gift),
            3 => Gifts::Creativity(gift),
            4 => Gifts::Growth(gift),
            5 => Gifts::Stability(gift),
            6 => Gifts::Success(gift),
            7 => Gifts::Knowledge(gift),
            8 => Gifts::MaterialSuccess(gift),
            _ => return Err(NumerolibError::OutOfRangeError(gift as usize)),
        })
    }

    /// Convert to numeric gift
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::challenges::birthday_gift::Gifts;
    /// let gift = Gifts::from_gift(5).unwrap();
    /// assert_eq!(gift.to_gift(), 5);
    /// ```
    ///
    pub fn to_gift(&self) -> u32 {
        match self {
            Gifts::Attainment(challenge) |
            Gifts::Intelligence(challenge) |
            Gifts::Creativity(challenge) |
            Gifts::Growth(challenge) |
            Gifts::Stability(challenge) |
            Gifts::Success(challenge) |
            Gifts::Knowledge(challenge) |
            Gifts::MaterialSuccess(challenge) => challenge.to_owned(),
        }
    }
}

impl fmt::Display for Gifts {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Gifts::Attainment(_) => write!(f, "Attainment"),
            Gifts::Intelligence(_) => write!(f, "Intelligence"),
            Gifts::Creativity(_) => write!(f, "Creativity"),
            Gifts::Growth(_) => write!(f, "Growth"),
            Gifts::Stability(_) => write!(f, "Stability"),
            Gifts::Success(_) => write!(f, "Success"),
            Gifts::Knowledge(_) => write!(f, "Knowledge"),
            Gifts::MaterialSuccess(_) => write!(f, "Material Success"),
        }
    }
}

/// Get the birthday gift, if any, from the birthdate challenge
///
/// # Arguments
///
/// * `Challenge` - The optional challenge
///
/// # Examples
///
/// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 6, THE BIRTHDAY GIFT
///
/// ```
/// use numerolib::date::challenges::{birthdate, birthday_gift};
/// use numerolib::date::basic_date::BasicDate;
/// let date = BasicDate::new("9/24/1941").unwrap();
/// match birthday_gift::gift( &birthdate::challenge(&date)) {
///     Some(gift) => assert_eq!(gift.to_gift(), 7),
///     None => panic!("No gift found")
/// }
/// ```
///
/// ```
/// use numerolib::date::challenges::{birthdate, birthday_gift};
/// use numerolib::date::basic_date::BasicDate;
/// let date = BasicDate::new("9/2/1941").unwrap();
/// match birthday_gift::gift( &birthdate::challenge(&date)) {
///     Some(_) => panic!("Unexpected gift found"),
///     None => assert!(true),
/// }
/// ```
///
/// ```
/// use numerolib::date::challenges::{birthdate, birthday_gift};
/// use numerolib::date::basic_date::BasicDate;
/// let date = BasicDate::new("9/22/1941").unwrap();
/// match birthday_gift::gift( &birthdate::challenge(&date)) {
///     Some(_) => panic!("Unexpected gift found"),
///     None => assert!(true),
/// }
/// ```
///
pub fn gift(phrase: &Option<Phrases>) -> Option<Gifts> {
    match phrase {
        None => None,
        Some(Phrases::No(_)) => None,
        Some(p) => Some(Gifts::from_gift(9 - p.to_challenge()).unwrap()),
    }
}


