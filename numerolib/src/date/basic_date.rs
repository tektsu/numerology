#![warn(missing_docs)]

//! # date:basic_date
//!
//! The date:basic_date module implements the Basic struct

use std::fmt;

use chrono::Datelike;
use chrono::naive::NaiveDate;
use string_builder::Builder;

use crate::date::universal_year_from_string;
use crate::error::NumerolibError;
use crate::word::Word;

/// A BasicDate holds the date and its immediate reductions
pub struct BasicDate {
    /// The parsed date
    pub date: NaiveDate,
    /// The reduction of the year
    pub year: Word,
    /// The reduction of the month
    pub month: Word,
    /// The reduction of the day
    pub day: Word,
    /// The Path of Life or Destiny for this date
    pub path_of_life: Word,
    /// The vibration octave
    pub vibration_octave: u32,
}

impl BasicDate {
    /// Create a new ['BasicDate'] instance
    ///
    /// # Arguments
    ///
    /// `date_in` - The date to be reduced; must be in the form "mm/dd/yyyy"
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("1/24/1941").unwrap();
    /// assert_eq!(date.date(), "01/24/1941");
    /// assert_eq!(date.year.word, "1941");
    /// assert_eq!(date.year.result, 6);
    /// assert_eq!(date.year.result_str, "6");
    /// assert_eq!(date.month.word, "1");
    /// assert_eq!(date.month.result, 1);
    /// assert_eq!(date.month.result_str, "1");
    /// assert_eq!(date.day.word, "24");
    /// assert_eq!(date.day.result, 6);
    /// assert_eq!(date.day.result_str, "6");
    /// assert_eq!(date.path_of_life.result, 22);
    /// assert_eq!(date.path_of_life.result_str, "22/4");
    /// ```
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 2, THE PATH OF LIFE
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let d = BasicDate::new("9/24/1941").unwrap();
    /// assert_eq!(d.path_of_life.result, 3);
    /// assert_eq!(d.path_of_life.result_str, "3");
    /// ```
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let d = BasicDate::new("1/24/1941").unwrap();
    /// assert_eq!(d.path_of_life.result, 22);
    /// assert_eq!(d.path_of_life.result_str, "22/4");
    /// ```
    ///
    pub fn new<S: Into<String>>(date_in: S) -> Result<Self, NumerolibError> {
        let input_date = date_in.into();
        let date = match NaiveDate::parse_from_str(input_date.as_str(), "%m/%d/%Y") {
            Ok(d) => d,
            Err(e) => return Err(NumerolibError::InvalidDate(format!("Date {}: {e}", input_date.as_str())))
        };

        // Reduce date components
        let year = Word::new_from_u32(date.year() as u32);
        let month = Word::new_from_u32(date.month() as u32);
        let day = Word::new_from_u32(date.day() as u32);

        // Calculate the Path of Life or Destiny (reduced full date)
        let path_of_life = Word::new_from_u32(date.day() + date.month() + date.year() as u32);

        let vibration_octave = match Self::vibration_octave(date.day()) {
            Ok(octave) => octave,
            Err(e) => return Err(e),
        };

        Ok(Self { date, year, month, day, path_of_life, vibration_octave })
    }

    /// Calculate the vibration octave
    ///
    /// * `day` - The day of month
    ///
    fn vibration_octave(day: u32) -> Result<u32, NumerolibError> {
        Ok(
            match day {
                1..=9 => 0,
                10..=18 => 1,
                19..=27 => 2,
                28..=31 => 3,
                _ => return Err(NumerolibError::InvalidDate(format!("{} is not a valid day number", day))),
            }
        )
    }

    /// Get the birthday vibration
    ///
    /// Returns a tuple with the reduced day number and the octave (first octave is 0)
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 4, THE VIBRATIONS OF THE BIRTHDAY
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/2/1941").unwrap();
    /// let (vibration, octave) = date.birthday_vibration();
    /// assert_eq!(vibration, 2);
    /// assert_eq!(octave, 0);
    /// ```
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/17/1941").unwrap();
    /// let (vibration, octave) = date.birthday_vibration();
    /// assert_eq!(vibration, 8);
    /// assert_eq!(octave, 1);
    /// ```
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// let (vibration, octave) = date.birthday_vibration();
    /// assert_eq!(vibration, 6);
    /// assert_eq!(octave, 2);
    /// ```
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/30/1941").unwrap();
    /// let (vibration, octave) = date.birthday_vibration();
    /// assert_eq!(vibration, 3);
    /// assert_eq!(octave, 3);
    /// ```
    pub fn birthday_vibration(&self) -> (u32, u32) {
        (self.day.result, self.vibration_octave)
    }

    /// Get the personal year for this birthdate and the specified year
    ///
    /// # Arguments
    ///
    /// `year` - The year from which to calculate Personal Year
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 3, THE LIFE CYCLES OR SUB-PATHS
    /// and Ch 10, THE PERSONAL VIBRATIONS
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// assert_eq!(date.personal_year(1973).result, 8);
    /// ```
    ///
    pub fn personal_year(&self, year: u32) -> Word {
        self.personal_year_from_string(year.to_string())
    }

    /// Get the personal year for this birthdate and the specified year string (there is no check to ensure the string is a valid year)
    ///
    /// # Arguments
    ///
    /// `year` - The year from which to calculate Personal Year
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 3, THE LIFE CYCLES OR SUB-PATHS
    /// and Ch 10, THE PERSONAL VIBRATIONS
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// let personal_year = date.personal_year_from_string("1973");
    /// assert_eq!(personal_year.result, 8);
    /// ```
    ///
    pub fn personal_year_from_string<S: Into<String>>(&self, year: S) -> Word {
        let universal_year = universal_year_from_string(year);

        Word::new(format!("{}{}{}", universal_year.fully_reduced_result, self.month.result_str, self.day.result_str))
    }

    /// Get the personal month for this birthdate and the given month and year
    ///
    /// # Arguments
    ///
    /// * `month` = The month
    /// * `year` - The year
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 10, THE PERSONAL VIBRATIONS
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// let personal_month = date.personal_month(10, 1973);
    /// assert_eq!(personal_month.result, 9);
    /// ```
    ///
    pub fn personal_month(&self, month: u32, year: u32) -> Word {
        let personal_year = &self.personal_year(year);
        let this_month = Word::new_from_u32(month);

        Word::new(format!("{}{}", this_month.fully_reduced_result, personal_year.fully_reduced_result))
    }

    /// Get the personal day for this birthdate and the given day, month and year
    ///
    /// # Arguments
    ///
    /// * `day` - The day
    /// * `month` = The month
    /// * `year` - The year
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 10, THE PERSONAL VIBRATIONS
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// let personal_month = date.personal_day(22, 10, 1973);
    /// assert_eq!(personal_month.result, 4);
    /// ```
    ///
    pub fn personal_day(&self, day: u32, month: u32, year: u32) -> Word {
        let personal_month = &self.personal_month(month, year);
        let today = Word::new_from_u32(day);

        Word::new(format!("{}{}", today.fully_reduced_result, personal_month.fully_reduced_result))
    }

    /// Get the date as a string
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// assert_eq!(date.date(), "09/24/1941");
    /// ```
    ///
    pub fn date(&self) -> String {
        self.date.format("%m/%d/%Y").to_string()
    }

    /// Get the year as an integer
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// assert_eq!(date.year(), 1941);
    /// ```
    ///
    pub fn year(&self) -> u32 {
        self.date.year() as u32
    }

    /// Get the month as an integer
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// assert_eq!(date.month(), 9);
    /// ```
    ///
    pub fn month(&self) -> u32 {
        self.date.month() as u32
    }

    /// Get the day as an integer
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// assert_eq!(date.day(), 24);
    /// ```
    ///
    pub fn day(&self) -> u32 {
        self.date.day() as u32
    }

    /// Get age at birthday in given year
    ///
    /// # Arguments
    ///
    /// `year` - The requested year
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::Date;
    /// let date = Date::new("9/24/1941").unwrap();
    /// match date.basic.age_in(1947) {
    ///     Ok(age) => assert_eq!(age, 6),
    ///     Err(year) => panic!("{} is before the birth year", year),
    /// }
    /// assert_eq!(date.basic.age_in(1947).unwrap(), 6);
    /// ```
    ///
    pub fn age_in(&self, year: u32) -> Result<u32, NumerolibError> {
        if year < self.year() {
            return Err(NumerolibError::AgeBeforeBirth(year));
        }

        Ok(year - self.year())
    }

    /// Get a string representation of this Date
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// let date_string = BasicDate::new("9/24/1941").unwrap().to_string();
    /// ```
    ///
    pub fn to_string(&self) -> String {
        let mut builder = Builder::default();
        builder.append(format!("{}\n", self.date()));
        builder.append(format!("  Year:  {}\n", self.year.reduction_str));
        builder.append(format!("  Month: {}\n", self.month.reduction_str));
        builder.append(format!("  Day:   {}\n", self.day.reduction_str));
        builder.append(format!("  Path of Life: {}\n", self.path_of_life.reduction_str));

        builder.string().unwrap().trim().to_string()
    }
}

impl fmt::Display for BasicDate {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

//==================================================================================================
// Unit Tests
//==================================================================================================

#[cfg(test)]
mod tests {
    use crate::date::Date;

    use super::*;

    #[test]
    fn test_new_basic_date() {
        struct WordDef<'a> {
            word: &'a str,
            result_str: &'a str,
            result: u32,
        }

        struct TestDef<'a> {
            date: &'a str,
            year: WordDef<'a>,
            month: WordDef<'a>,
            day: WordDef<'a>,
            err: bool,
        }

        let table = vec![
            TestDef {
                date: "05/06/1959",
                year: WordDef { word: "1959", result_str: "6", result: 6 },
                month: WordDef { word: "5", result_str: "5", result: 5 },
                day: WordDef { word: "6", result_str: "6", result: 6 },
                err: false,
            },
            TestDef {
                date: "01/24/1941",
                year: WordDef { word: "1941", result_str: "6", result: 6 },
                month: WordDef { word: "1", result_str: "1", result: 1 },
                day: WordDef { word: "24", result_str: "6", result: 6 },
                err: false,
            },
            TestDef {
                date: "09/31/1941",
                year: WordDef { word: "1941", result_str: "6", result: 6 },
                month: WordDef { word: "9", result_str: "9", result: 9 },
                day: WordDef { word: "31", result_str: "4", result: 4 },
                err: true,
            },
        ];

        for test in table {
            let run_tests = |date: BasicDate, note: &str| {
                assert_eq!(date.date(), test.date, "date: [{}] note: [{}]", test.date, note);
                assert_eq!(date.year.word, test.year.word, "date: [{}] note: [{}]", test.date, note);
                assert_eq!(date.year.result_str, test.year.result_str, "date: [{}] note: [{}]", test.date, note);
                assert_eq!(date.year.result, test.year.result, "date: [{}] note: [{}]", test.date, note);
                assert_eq!(date.month.word, test.month.word, "date: [{}] note: [{}]", test.date, note);
                assert_eq!(date.month.result_str, test.month.result_str, "date: [{}] note: [{}]", test.date, note);
                assert_eq!(date.month.result, test.month.result, "date: [{}] note: [{}]", test.date, note);
                assert_eq!(date.day.word, test.day.word, "date: [{}] note: [{}]", test.date, note);
                assert_eq!(date.day.result_str, test.day.result_str, "date: [{}] note: [{}]", test.date, note);
                assert_eq!(date.day.result, test.day.result, "date: [{}] note: [{}]", test.date, note);
            };

            let check_results = |date_result: Result<BasicDate, NumerolibError>| {
                match date_result {
                    Ok(date) => {
                        if test.err {
                            panic!("Reducing date should have failed");
                        } else {
                            run_tests(date, "as str");
                        }
                    }
                    Err(e) => {
                        if test.err {
                            match e {
                                NumerolibError::InvalidDate(_) => {}
                                _ => { panic!("Test should have failed with NumerolibError::InvalidDate()") }
                            }
                        } else {
                            panic!("Reducing date should have passed: {e}");
                        }
                    }
                };
            };

            check_results(BasicDate::new(test.date));
            check_results(BasicDate::new(test.date.to_string()));
        }
    }

    #[test]
    fn test_path_of_life() {
        struct TestDef<'a> {
            date: &'a str,
            path_of_life_str: &'a str,
            path_of_life: u32,
        }

        let table = vec![
            TestDef { date: "1/24/1941", path_of_life_str: "22/4", path_of_life: 22 },
            TestDef { date: "9/24/1941", path_of_life_str: "3", path_of_life: 3 },
            TestDef { date: "2/2/1973", path_of_life_str: "6", path_of_life: 6 },
        ];

        for test in table {
            let date = BasicDate::new(test.date).unwrap();
            assert_eq!(date.path_of_life.result, test.path_of_life, "date: [{}]", test.date);
            assert_eq!(date.path_of_life.result_str, test.path_of_life_str, "date: [{}]", test.date);
        }
    }

    #[test]
    fn test_age_in() {
        struct TestDef<'a> {
            date: &'a str,
            year: u32,
            age: u32,
            err: bool,
        }

        let table = vec![
            TestDef { date: "9/24/1941", year: 1942, age: 1, err: false },
            TestDef { date: "9/24/1941", year: 1966, age: 25, err: false },
            TestDef { date: "9/24/1941", year: 2002, age: 61, err: false },
            TestDef { date: "9/24/1941", year: 1941, age: 0, err: false },
            TestDef { date: "9/24/1941", year: 1940, age: 0, err: true },
        ];

        for test in table {
            let date_result = Date::new(test.date);
            let date = match date_result {
                Ok(rd) => rd,
                Err(e) => panic!("Invalid Date: {}", e.to_string()),
            };
            if test.err {
                match date.basic.age_in(test.year) {
                    Ok(_) => panic!("date: [{}] year[{}] This test should have matched Err()", test.date, test.year),
                    Err(e) => {
                        match e {
                            NumerolibError::AgeBeforeBirth(year) => {
                                assert_eq!(year, test.year)
                            }
                            _ => { panic!("Error should have been NumerolibError::AgeBeforeBirth()") }
                        }
                    }
                }
            } else {
                let age = date.basic.age_in(test.year).unwrap();
                assert_eq!(age, test.age);
            }
        }
    }

    #[test]
    fn test_personal_year() {
        struct TestDef<'a> {
            date: &'a str,
            year: u32,
            personal_year_str: &'a str,
            personal_year: u32,
        }

        let table = vec![
            TestDef { date: "9/24/1941", year: 1973, personal_year_str: "8", personal_year: 8 },
        ];

        for test in table {
            let run_tests = |date: Word, note: &str| {
                assert_eq!(date.result_str, test.personal_year_str, "date: [{}] year: [{}] note: [{}]", test.date, test.year, note);
                assert_eq!(date.result, test.personal_year, "date: [{}] year: [{}] note: [{}]", test.date, test.year, note);
            };
            let date = Date::new(test.date).unwrap();
            run_tests(date.basic.personal_year(test.year), "as u32");
            run_tests(date.basic.personal_year_from_string(test.year.to_string()), "as string");
            run_tests(date.basic.personal_year_from_string(test.year.to_string().as_str()), "as str");
        }
    }

    #[test]
    fn test_birthday_vibration() {
        for day in 1..=31 {
            let date = BasicDate::new(format!("12/{day}/1941")).unwrap();
            let (_, octave) = date.birthday_vibration();
            let correct_octave = match day {
                1..=9 => 0,
                10..=18 => 1,
                19..=27 => 2,
                28..=31 => 3,
                _ => { panic!("Invalid day {day}") }
            };
            assert_eq!(octave, correct_octave, "day: {}", day);
        }
    }
}