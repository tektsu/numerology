#![warn(missing_docs)]

//! # date:path_of_life
//!
//! The date:path_of_life module implements the Path of Life keywords

pub mod positive;

pub mod negative;