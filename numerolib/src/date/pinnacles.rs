#![warn(missing_docs)]

//! # date:pinnacles
//!
//! The date:pinnacles module implements the Pinnacles struct and related structs

use std::fmt;

use string_builder::Builder;

use crate::date::basic_date::BasicDate;
use crate::date::pinnacles::attainment_cycles::Cycles;
use crate::word::Word;

pub mod attainment_cycles;

/// The four pinnacles
pub enum PinnacleType {
    /// The first Pinnacle - Attainment
    Attainment,
    /// The 2nd Pinnacle - Obligation
    Obligation,
    /// The 3rd Pinnacle - Foundation
    Foundation,
    /// The 4th Pinnacle - Retrospection
    Retrospection,
}

/// A Pinnacle struct holds the type and year of an attainment
pub struct Pinnacle {
    /// The type of pinnacle
    pub pinnacle_type: PinnacleType,
    /// The pinnacle
    pub pinnacle: Word,
    /// The year of the pinnacle
    pub year: u32,
    /// Age at this pinnacle
    pub age: u32,
}

//==================================================================================================
// Pinnacles
//==================================================================================================

/// The Pinnacles struct holds and instance of each of the the four pinnacles
pub struct Pinnacles {
    /// The Pinnacle of Attainment
    pub attainment: Pinnacle,
    /// The Pinnacle of Obligation
    pub obligation: Pinnacle,
    /// The Pinnacle of Foundation
    pub foundation: Pinnacle,
    /// The Pinnacle of Retrospection
    pub retrospection: Pinnacle,
    /// The Attainment Cycles
    pub cycles: Cycles,
}

impl Pinnacles {
    /// Create a new ['Pinnacles'] instance
    ///
    /// # Arguments
    /// `date` - A BasicDate reference for which to calculate the Pinnacles
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 7, THE ATTAINMENT
    ///
    /// ```
    /// use numerolib::date::Date;
    /// let date = Date::new("9/24/1941").unwrap();
    /// assert_eq!(date.pinnacles.attainment.pinnacle.result, 6);
    /// assert_eq!(date.pinnacles.obligation.year, 1974);
    /// assert_eq!(date.pinnacles.obligation.age, 33);
    /// assert_eq!(date.pinnacles.obligation.pinnacle.result, 3);
    /// assert_eq!(date.pinnacles.foundation.year, 1983);
    /// assert_eq!(date.pinnacles.foundation.age, 42);
    /// assert_eq!(date.pinnacles.foundation.pinnacle.result, 9);
    /// assert_eq!(date.pinnacles.retrospection.year, 1992);
    /// assert_eq!(date.pinnacles.retrospection.age, 51);
    /// assert_eq!(date.pinnacles.retrospection.pinnacle.result, 6);
    /// ```
    ///
    pub fn new(date: &BasicDate) -> Pinnacles {
        let attainment = Pinnacle {
            pinnacle_type: PinnacleType::Attainment,
            pinnacle: Word::new_from_u32(date.month.result + date.day.result),
            year: date.year(),
            age: 0,
        };
        let obligation = Pinnacle {
            pinnacle_type: PinnacleType::Obligation,
            pinnacle: Word::new_from_u32(date.year.result + date.day.result),
            year: date.year() + (36 - date.path_of_life.result),
            age: date.age_in(date.year() + (36 - date.path_of_life.result)).unwrap(),
        };
        let foundation = Pinnacle {
            pinnacle_type: PinnacleType::Foundation,
            pinnacle: Word::new_from_u32(attainment.pinnacle.result + obligation.pinnacle.result),
            year: obligation.year + 9,
            age: date.age_in(obligation.year + 9).unwrap(),
        };
        let retrospection = Pinnacle {
            pinnacle_type: PinnacleType::Retrospection,
            pinnacle: Word::new_from_u32(date.year.result + date.month.result),
            year: foundation.year + 9,
            age: date.age_in(foundation.year + 9).unwrap(),
        };
        let cycles = Cycles::new(date);

        Pinnacles { attainment, obligation, foundation, retrospection, cycles }
    }

    /// Represent a Pinnacles as a String
    pub fn to_string(&self) -> String {
        let mut builder = Builder::default();
        builder.append(format!("Pinnacle of Attainment begins in {} at birth: {}\n", self.attainment.year, self.attainment.pinnacle.result_str));
        builder.append(format!("Pinnacle of Obligation begins in {} at age {}: {}\n", self.obligation.year, self.obligation.age, self.obligation.pinnacle.result_str));
        builder.append(format!("Pinnacle of Foundation begins in {} at age {}: {}\n", self.foundation.year, self.foundation.age, self.foundation.pinnacle.result_str));
        builder.append(format!("Pinnacle of Retrospection begins in {} at age {}: {}\n", self.retrospection.year, self.retrospection.age, self.retrospection.pinnacle.result_str));
        builder.append("Attainment Cycles\n");
        for i in 1..=11 {
            let cycle_type = self.cycles.get(i as u32).unwrap();
            let start_year = cycle_type.to_start_year();
            builder.append(format!("  {:2} - {} to {} - {}\n", i, start_year, start_year + 9, cycle_type.to_string()));
        }
        builder.string().unwrap().trim().to_string()
    }
}

impl fmt::Display for Pinnacles {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

#[cfg(test)]
mod tests {
    use crate::date::basic_date::BasicDate;
    use crate::date::Date;
    use crate::date::pinnacles::attainment_cycles::types::Types;
    use crate::error::NumerolibError;

    #[test]
    fn test_pinnacles() {
        struct TestDef<'a> {
            date: &'a str,
            attainment: u32,
            obligation: u32,
            obligation_year: u32,
            foundation: u32,
            foundation_year: u32,
            retrospection: u32,
            retrospection_year: u32,
        }

        let table = vec![
            TestDef { date: "9/24/1941", attainment: 6, obligation: 3, obligation_year: 1974, foundation: 9, foundation_year: 1983, retrospection: 6, retrospection_year: 1992 },
        ];

        for test in table {
            let date = Date::new(test.date).unwrap();
            assert_eq!(date.pinnacles.attainment.pinnacle.result, test.attainment);
            assert_eq!(date.pinnacles.obligation.year, test.obligation_year);
            assert_eq!(date.pinnacles.obligation.pinnacle.result, test.obligation);
            assert_eq!(date.pinnacles.foundation.year, test.foundation_year);
            assert_eq!(date.pinnacles.foundation.pinnacle.result, test.foundation);
            assert_eq!(date.pinnacles.retrospection.year, test.retrospection_year);
            assert_eq!(date.pinnacles.retrospection.pinnacle.result, test.retrospection);
        }
    }

    #[test]
    fn test_attainment_cycle() {
        let basic_date = BasicDate::new("9/24/1941").unwrap();
        match Types::new(0, &basic_date) {
            Ok(_) => panic!("Types 0 should have failed"),
            Err(e) => {
                match e {
                    NumerolibError::OutOfRangeError(i) => assert_eq!(i, 0),
                    _ => panic!("Test should have failed with NumerolibError::OutOfRangeError()"),
                }
            }
        }
    }

    #[test]
    fn test_attainment_cycles() {
        struct TestDef<'a> {
            date: &'a str,
        }

        let table = vec![
            TestDef { date: "9/24/1941" },
            TestDef { date: "5/6/1959" },
        ];

        for test in table {
            let date = Date::new(test.date).unwrap();
            for i in 1..=11 {
                let cycle_type = date.pinnacles.cycles.get(i).unwrap();
                assert_eq!(cycle_type.to_cycle().num, i as u32, "date: [{}]", date.basic.date());
                assert_eq!(cycle_type.to_start_year(), date.basic.year() + 9 * (i - 1));
                assert_eq!(cycle_type.to_string(), match i {
                    1 => "Individualization",
                    2 => "Association",
                    3 => "Self-expression",
                    4 => "Work",
                    5 => "Freedom",
                    6 => "Responsibility",
                    7 => "Introspection",
                    8 => "MaterialAspects",
                    9 => "Encompassing",
                    10 => "Rebirth",
                    11 => "Inspiration",
                    _ => panic!("Invalid attainment cycle keyword: [{}] num: [{}] date: [{}]", date.pinnacles.cycles.get(i).unwrap().to_string(), i, date.basic.date()),
                })
            }
        }
    }
}