#![warn(missing_docs)]

//! # date:path_of_life::positive
//!
//! The date:path_of_life module implements the Path of Life positive keywords

use std::fmt;

use crate::error::NumerolibError;

/// Positive Path of Life Keywords
#[allow(missing_docs)]
#[derive(Debug)]
pub enum Positive {
    Individual(u32),
    Association(u32),
    Pleasant(u32),
    Organization(u32),
    Freedom(u32),
    Power(u32),
    Wisdom(u32),
    Material(u32),
    Encompassing(u32),
    Inspiration(u32),
    Universal(u32),
}

impl Positive {
    /// Get the positive path of life keyword
    ///
    /// # Arguments
    ///
    /// * `pol` - Path of Life
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::path_of_life;
    /// let keyword = path_of_life::positive::Positive::from_path_of_life(5).unwrap();
    /// assert_eq!(keyword.to_string(), "Freedom");
    /// ```
    ///
    pub fn from_path_of_life(pol: u32) -> Result<Positive, NumerolibError> {
        Ok(match pol {
            1 => Positive::Individual(pol),
            2 => Positive::Association(pol),
            3 => Positive::Pleasant(pol),
            4 => Positive::Organization(pol),
            5 => Positive::Freedom(pol),
            6 => Positive::Power(pol),
            7 => Positive::Wisdom(pol),
            8 => Positive::Material(pol),
            9 => Positive::Encompassing(pol),
            11 => Positive::Inspiration(pol),
            22 => Positive::Universal(pol),
            _ => return Err(NumerolibError::OutOfRangeError(pol as usize))
        })
    }

    /// Convert to Path of Life
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::path_of_life;
    /// let keyword = path_of_life::positive::Positive::from_path_of_life(5).unwrap();
    /// assert_eq!(keyword.to_path_of_life(), 5);
    /// ```
    ///
    pub fn to_path_of_life(self) -> u32 {
        match self {
            Positive::Individual(pol) => pol,
            Positive::Association(pol) => pol,
            Positive::Pleasant(pol) => pol,
            Positive::Organization(pol) => pol,
            Positive::Freedom(pol) => pol,
            Positive::Power(pol) => pol,
            Positive::Wisdom(pol) => pol,
            Positive::Material(pol) => pol,
            Positive::Encompassing(pol) => pol,
            Positive::Inspiration(pol) => pol,
            Positive::Universal(pol) => pol,
        }
    }
}

impl fmt::Display for Positive {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Positive::Individual(_) => write!(f, "Individual"),
            Positive::Association(_) => write!(f, "Association"),
            Positive::Pleasant(_) => write!(f, "Pleasant"),
            Positive::Organization(_) => write!(f, "Organization"),
            Positive::Freedom(_) => write!(f, "Freedom"),
            Positive::Power(_) => write!(f, "Power/Security"),
            Positive::Wisdom(_) => write!(f, "Wisdom"),
            Positive::Material(_) => write!(f, "Material"),
            Positive::Encompassing(_) => write!(f, "Encompassing"),
            Positive::Inspiration(_) => write!(f, "Inspiration"),
            Positive::Universal(_) => write!(f, "Universal"),
        }
    }
}


