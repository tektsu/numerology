#![warn(missing_docs)]

//! # date:path_of_life::negative
//!
//! The date:path_of_life module implements the Path of Life negative keywords

use std::fmt;

use crate::error::NumerolibError;

/// Negative Path of Life Keywords
#[allow(missing_docs)]
#[derive(Debug)]
pub enum Negative {
    Yourself(u32),
    Subservience(u32),
    Expression(u32),
    Work(u32),
    Change(u32),
    Adjustment(u32),
    Understanding(u32),
    Control(u32),
    Denial(u32),
    Sacrifice(u32),
    Ideals(u32),
}

impl Negative {
    /// Get the negative path of life keyword
    ///
    /// # Arguments
    ///
    /// * `pol` - Path of Life
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::path_of_life;
    /// let keyword = path_of_life::negative::Negative::from_path_of_life(5).unwrap();
    /// assert_eq!(keyword.to_string(), "Change");
    /// ```
    ///
    pub fn from_path_of_life(pol: u32) -> Result<Negative, NumerolibError> {
        Ok(match pol {
            1 => Negative::Yourself(pol),
            2 => Negative::Subservience(pol),
            3 => Negative::Expression(pol),
            4 => Negative::Work(pol),
            5 => Negative::Change(pol),
            6 => Negative::Adjustment(pol),
            7 => Negative::Understanding(pol),
            8 => Negative::Control(pol),
            9 => Negative::Denial(pol),
            11 => Negative::Sacrifice(pol),
            22 => Negative::Ideals(pol),
            _ => return Err(NumerolibError::OutOfRangeError(pol as usize))
        })
    }

    /// Convert to Path of Life
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::path_of_life;
    /// let keyword = path_of_life::negative::Negative::from_path_of_life(5).unwrap();
    /// assert_eq!(keyword.to_path_of_life(), 5);
    /// ```
    ///
    pub fn to_path_of_life(self) -> u32 {
        match self {
            Negative::Yourself(pol) => pol,
            Negative::Subservience(pol) => pol,
            Negative::Expression(pol) => pol,
            Negative::Work(pol) => pol,
            Negative::Change(pol) => pol,
            Negative::Adjustment(pol) => pol,
            Negative::Understanding(pol) => pol,
            Negative::Control(pol) => pol,
            Negative::Denial(pol) => pol,
            Negative::Sacrifice(pol) => pol,
            Negative::Ideals(pol) => pol,
        }
    }
}

impl fmt::Display for Negative {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Negative::Yourself(_) => write!(f, "Self"),
            Negative::Subservience(_) => write!(f, "Subservience"),
            Negative::Expression(_) => write!(f, "Expression"),
            Negative::Work(_) => write!(f, "Work"),
            Negative::Change(_) => write!(f, "Change"),
            Negative::Adjustment(_) => write!(f, "Adjustment"),
            Negative::Understanding(_) => write!(f, "Understanding"),
            Negative::Control(_) => write!(f, "Control"),
            Negative::Denial(_) => write!(f, "Denial"),
            Negative::Sacrifice(_) => write!(f, "Sacrifice"),
            Negative::Ideals(_) => write!(f, "Ideals"),
        }
    }
}
