#![warn(missing_docs)]

//! # date:cycles
//!
//! The date:cycles module implements the Cycles struct and related structs

use std::fmt;

use string_builder::Builder;

use crate::date::basic_date::BasicDate;
use crate::date::path_of_life::positive;
use crate::word::Word;

/// CycleType describes the three different kinds of cycles
pub enum CycleType {
    /// The 1st Cycle - Formative
    Formative,
    /// The 2nd Cycle - Productive
    Productive,
    /// The 3rd Cycle - Harvest
    Harvest,
}

/// A Cycle describes one of the life cycles
pub struct Cycle {
    /// The cycle type
    cycle_type: CycleType,
    /// The year the cycle begins
    pub beginning_year: u32,
    /// Age in beginning year
    pub age: u32,
    /// The cycle value
    pub cycle: Word,
}

impl Cycle {
    /// Create a new ['Cycle'] instance
    ///
    /// # Arguments
    /// `date` - Reference to a ['Date'] instance
    /// `cycle_type` - The type of Cycle to create
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::*;
    /// use numerolib::date::basic_date::BasicDate;
    /// use numerolib::date::cycles::{Cycle, CycleType};
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// let formative = Cycle::new(&date, CycleType::Formative);
    /// assert_eq!(formative.beginning_year, 1941);
    /// assert_eq!(formative.cycle.result, 9);
    /// ```
    ///
    pub fn new(date: &BasicDate, cycle_type: CycleType) -> Cycle {
        match cycle_type {
            CycleType::Formative => {
                Cycle {
                    cycle_type,
                    cycle: Word::new_from_u32(date.month()),
                    beginning_year: date.year(),
                    age: 0,
                }
            }
            CycleType::Productive => {
                let year = date.year() + 28;
                let personal_year = date.personal_year(year);
                let productive_year = match personal_year.fully_reduced_result {
                    2..=5 => year - (personal_year.fully_reduced_result - 1),
                    6..=9 => year + (10 - personal_year.fully_reduced_result),
                    _ => year,
                };
                Cycle {
                    cycle_type,
                    cycle: Word::new_from_u32(date.day()),
                    beginning_year: productive_year,
                    age: date.age_in(productive_year).unwrap(),
                }
            }
            CycleType::Harvest => {
                let year = date.year() + 57;
                let personal_year = date.personal_year(year);
                let harvest_year = match personal_year.fully_reduced_result {
                    2..=5 => year - (personal_year.fully_reduced_result - 1),
                    6..=9 => year + (10 - personal_year.fully_reduced_result),
                    _ => year,
                };
                Cycle {
                    cycle_type,
                    cycle: Word::new_from_u32(date.year()),
                    beginning_year: harvest_year,
                    age: date.age_in(harvest_year).unwrap(),
                }
            }
        }
    }

    /// Get the name of a cycle given the CycleType
    ///
    /// # Arguments
    /// `cycle_type` - The type of cycle from the CycleType enum
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// use numerolib::date::cycles::{Cycle, CycleType};
    ///assert_eq!(Cycle::cycle_name(CycleType::Formative), "Formative");
    /// ```
    pub fn cycle_name(cycle_type: CycleType) -> String {
        match cycle_type {
            CycleType::Formative => "Formative".to_string(),
            CycleType::Productive => "Productive".to_string(),
            CycleType::Harvest => "Harvest".to_string(),
        }
    }

    /// Represent a Cycle as a String
    pub fn to_string(&self) -> String {
        let mut builder = Builder::default();

        match self.cycle_type {
            CycleType::Formative => {
                builder.append(format!("Formative Cycle begins in {} at birth:  {} ({})", self.beginning_year, self.cycle.result_str, positive::Positive::from_path_of_life(self.cycle.result).unwrap()));
            }
            CycleType::Productive => {
                // let pcycle = date.productive_cycle();
                // let pcycle_begins = date.year_productive_cycle_begins();
                // let age = date.age_in(pcycle_begins).unwrap();
                // println!("  Productive Cycle begins in {} at age {}: {} ({})", pcycle_begins, age, pcycle, positive_keyword(pcycle).unwrap());
                builder.append(format!("Productive Cycle begins in {} at age {}:  {} ({})", self.beginning_year, self.age, self.cycle.result_str, positive::Positive::from_path_of_life(self.cycle.result).unwrap()));
            }
            CycleType::Harvest => {
                builder.append(format!("Harvest Cycle begins in {} at age {}:  {} ({})", self.beginning_year, self.age, self.cycle.result_str, positive::Positive::from_path_of_life(self.cycle.result).unwrap()));
            }
        }

        builder.string().unwrap().trim().to_string()
    }
}

impl fmt::Display for Cycle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

/// The Cycles structure holds the results for the three cycle calculations
pub struct Cycles {
    /// The Formative Cycle
    pub formative: Cycle,
    /// The Productive Cycle
    pub productive: Cycle,
    /// The Harvest Cycle
    pub harvest: Cycle,
}

impl Cycles {
    /// Create a new ['Cycles'] instance
    ///
    /// # Arguments
    /// `date` - Reference to a ['Date'] instance
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 3, THE LIFE CYCLES OR SUB-PATHS
    ///
    /// ```
    /// use numerolib::date::*;
    /// use numerolib::date::basic_date::BasicDate;
    /// use numerolib::date::cycles::Cycles;
    /// let date = BasicDate::new("9/24/1941").unwrap();
    /// let cycles = Cycles::new(&date);
    /// assert_eq!(cycles.formative.beginning_year, 1941);
    /// assert_eq!(cycles.formative.cycle.result, 9);
    /// assert_eq!(cycles.productive.beginning_year, 1966);
    /// assert_eq!(cycles.productive.cycle.result, 6);
    /// assert_eq!(cycles.harvest.beginning_year, 2002);
    /// assert_eq!(cycles.harvest.cycle.result, 6);
    /// ```
    ///
    pub fn new(date: &BasicDate) -> Cycles {
        Cycles {
            formative: Cycle::new(date, CycleType::Formative),
            productive: Cycle::new(date, CycleType::Productive),
            harvest: Cycle::new(date, CycleType::Harvest),
        }
    }

    /// Represent a Cycles as a String
    pub fn to_string(&self) -> String {
        let mut builder = Builder::default();
        builder.append(format!("{}\n", self.formative));
        builder.append(format!("{}\n", self.productive));
        builder.append(format!("{}\n", self.harvest));
        builder.string().unwrap().trim().to_string()
    }
}

impl fmt::Display for Cycles {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

//==================================================================================================
// Unit Tests
//==================================================================================================

#[cfg(test)]
mod tests {
    use crate::date::Date;

    #[test]
    fn test_cycles() {
        struct TestDef<'a> {
            date: &'a str,
            formative_cycle: u32,
            productive_cycle: u32,
            harvest_cycle: u32,
        }

        let table = vec![
            TestDef { date: "9/24/1935", formative_cycle: 9, productive_cycle: 6, harvest_cycle: 9 },
            TestDef { date: "9/24/1936", formative_cycle: 9, productive_cycle: 6, harvest_cycle: 1 },
            TestDef { date: "9/24/1937", formative_cycle: 9, productive_cycle: 6, harvest_cycle: 2 },
            TestDef { date: "9/24/1938", formative_cycle: 9, productive_cycle: 6, harvest_cycle: 3 },
            TestDef { date: "9/24/1939", formative_cycle: 9, productive_cycle: 6, harvest_cycle: 22 },
            TestDef { date: "9/24/1940", formative_cycle: 9, productive_cycle: 6, harvest_cycle: 5 },
            TestDef { date: "9/24/1941", formative_cycle: 9, productive_cycle: 6, harvest_cycle: 6 },
            TestDef { date: "1/24/1941", formative_cycle: 1, productive_cycle: 6, harvest_cycle: 6 },
            TestDef { date: "5/6/1959", formative_cycle: 5, productive_cycle: 6, harvest_cycle: 6 },
            TestDef { date: "2/2/1973", formative_cycle: 2, productive_cycle: 2, harvest_cycle: 2 },
        ];

        for test in table {
            let date = Date::new(test.date).unwrap();
            assert_eq!(date.cycles.formative.cycle.result, test.formative_cycle, "date: [{}] note: [formative]", test.date);
            assert_eq!(date.cycles.productive.cycle.result, test.productive_cycle, "date: [{}] note: [productive]", test.date);
            assert_eq!(date.cycles.harvest.cycle.result, test.harvest_cycle, "date: [{}] note: [harvest]", test.date);
        }
    }

    #[test]
    fn test_cycle_beginning_years() {
        struct TestDef<'a> {
            date: &'a str,
            formative_year: u32,
            productive_year: u32,
            harvest_year: u32,
        }

        let table = vec![
            TestDef { date: "9/24/1936", formative_year: 1936, productive_year: 1966, harvest_year: 1993 },
            TestDef { date: "9/24/1937", formative_year: 1937, productive_year: 1966, harvest_year: 1993 },
            TestDef { date: "9/24/1938", formative_year: 1938, productive_year: 1966, harvest_year: 1993 },
            TestDef { date: "9/24/1939", formative_year: 1939, productive_year: 1966, harvest_year: 1993 },
            TestDef { date: "9/24/1940", formative_year: 1940, productive_year: 1966, harvest_year: 1993 },
            TestDef { date: "9/24/1941", formative_year: 1941, productive_year: 1966, harvest_year: 2002 },
            TestDef { date: "9/24/1942", formative_year: 1942, productive_year: 1966, harvest_year: 2002 },
            TestDef { date: "9/24/1943", formative_year: 1943, productive_year: 1975, harvest_year: 2002 },
            TestDef { date: "9/24/1944", formative_year: 1944, productive_year: 1975, harvest_year: 2002 },
            TestDef { date: "9/24/1945", formative_year: 1945, productive_year: 1975, harvest_year: 2002 },
            TestDef { date: "9/24/1946", formative_year: 1946, productive_year: 1975, harvest_year: 2002 },
            TestDef { date: "5/6/1959", formative_year: 1959, productive_year: 1988, harvest_year: 2015 },
        ];

        for test in table {
            let date = Date::new(test.date).unwrap();
            assert_eq!(date.cycles.formative.beginning_year, test.formative_year, "date: [{}] note: [formative]", test.date);
            assert_eq!(date.cycles.productive.beginning_year, test.productive_year, "date: [{}] note: [productive]", test.date);
            assert_eq!(date.cycles.harvest.beginning_year, test.harvest_year, "date: [{}] note: [harvest]", test.date);
        }
    }
}