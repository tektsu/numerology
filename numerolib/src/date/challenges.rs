#![warn(missing_docs)]

//! # date:challenges
//!
//! The date:challenges module implements the Challenges struct

use string_builder::Builder;

use crate::date::basic_date::BasicDate;
use crate::date::challenges::birthdate::Phrases;
use crate::date::challenges::birthday_gift::Gifts;

pub mod birthdate;

pub mod birthday_gift;

/// The Challenges struct holds the various challenges and gifts
pub struct Challenges {
    /// The birthday challenge for the given date
    pub birthday_challenge: Option<Phrases>,
    /// The birthday gift for the given date
    pub birthday_gift: Option<Gifts>,
    /// The 1st minor challenge for the given date
    pub minor1: u32,
    /// The 2nd minor challenge for the given date
    pub minor2: u32,
    /// The major challenge for the given date
    pub major: u32,
    /// Any additional minor challenge for the given date
    pub additional_minor: Option<u32>,
}

impl Challenges {
    /// Create a new ['Challenges'] instance
    ///
    /// # Arguments
    /// `date` - A reference to the BasicDate from which to calculate the challenges
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 9, THE LIFE CHALLENGES
    ///
    /// ```
    /// use numerolib::date::*;
    /// let date = Date::new("9/24/1941").unwrap();
    /// assert_eq!(date.challenges.minor1, 3);
    /// assert_eq!(date.challenges.minor2, 0);
    /// assert_eq!(date.challenges.major, 3);
    /// match date.challenges.additional_minor {
    ///     Some(_) => panic!("Unexpected additional minor challenge"),
    ///     None => {}
    /// }
    /// ```
    ///
    /// ```
    /// use numerolib::date::*;
    /// let date = Date::new("1/12/1908").unwrap();
    /// assert_eq!(date.challenges.minor1, 2);
    /// assert_eq!(date.challenges.minor2, 6);
    /// assert_eq!(date.challenges.major, 4);
    /// match date.challenges.additional_minor {
    ///     Some(c) => { assert_eq!(c, 8); }
    ///     None => panic!("Missing additional minor challenge"),
    /// }
    /// ```
    ///
    pub fn new(date: &BasicDate) -> Challenges {
        let birthday_challenge = birthdate::challenge(&date);
        let birthday_gift = birthday_gift::gift(&birthday_challenge);
        let minor1 = (date.month.fully_reduced_result as i32 - date.day.fully_reduced_result as i32).abs() as u32;
        let minor2 = (date.year.fully_reduced_result as i32 - date.day.fully_reduced_result as i32).abs() as u32;
        let major = (minor1 as i32 - minor2 as i32).abs() as u32;
        let am = (date.year.fully_reduced_result as i32 - date.month.fully_reduced_result as i32).abs() as u32;
        let additional_minor = match am == major {
            true => None,
            false => Some(am),
        };

        Challenges {
            birthday_challenge,
            birthday_gift,
            minor1,
            minor2,
            major,
            additional_minor,
        }
    }
    /// Represent a Challenges as a String
    pub fn to_string(&self) -> String {
        let mut builder = Builder::default();
        builder.append("Birthday Challenge: ");
        builder.append(match self.birthday_challenge {
            None => "None\n".to_string(),
            Some(c) => { format!("{} ({})\n", c.to_challenge(), c.to_string()) }
        });
        builder.append("Birthday Gift: ");
        builder.append(match self.birthday_gift {
            None => "None\n".to_string(),
            Some(g) => { format!("{} ({})\n", g.to_gift(), g.to_string()) }
        });
        builder.append(format!("First Minor Challenge: {}\n", self.minor1));
        builder.append(format!("Second Minor Challenge: {}\n", self.minor2));
        builder.append(format!("Major Challenge: {}\n", self.major));
        match self.additional_minor {
            None => {}
            Some(am) => builder.append(format!("Additional Minor Challenge: {}\n", am)),
        }

        builder.string().unwrap().trim().to_string()
    }
}

//==================================================================================================
// Unit Tests
//==================================================================================================

#[cfg(test)]
mod tests {
    use crate::date::Date;

    #[test]
    fn test_birthday_gifts_and_challenges() {
        struct TestDef<'a> {
            date: &'a str,
            challenge: Option<u32>,
            gift: Option<u32>,
        }

        let table = vec![
            TestDef { date: "9/1/1941", challenge: None, gift: None },
            TestDef { date: "9/2/1941", challenge: None, gift: None },
            TestDef { date: "9/3/1941", challenge: None, gift: None },
            TestDef { date: "9/4/1941", challenge: None, gift: None },
            TestDef { date: "9/5/1941", challenge: None, gift: None },
            TestDef { date: "9/6/1941", challenge: None, gift: None },
            TestDef { date: "9/7/1941", challenge: None, gift: None },
            TestDef { date: "9/8/1941", challenge: None, gift: None },
            TestDef { date: "9/9/1941", challenge: None, gift: None },
            TestDef { date: "9/10/1941", challenge: Some(1), gift: Some(8) },
            TestDef { date: "9/11/1941", challenge: Some(0), gift: None },
            TestDef { date: "9/12/1941", challenge: Some(1), gift: Some(8) },
            TestDef { date: "9/13/1941", challenge: Some(2), gift: Some(7) },
            TestDef { date: "9/14/1941", challenge: Some(3), gift: Some(6) },
            TestDef { date: "9/15/1941", challenge: Some(4), gift: Some(5) },
            TestDef { date: "9/16/1941", challenge: Some(5), gift: Some(4) },
            TestDef { date: "9/17/1941", challenge: Some(6), gift: Some(3) },
            TestDef { date: "9/18/1941", challenge: Some(7), gift: Some(2) },
            TestDef { date: "9/19/1941", challenge: Some(8), gift: Some(1) },
            TestDef { date: "9/20/1941", challenge: Some(2), gift: Some(7) },
            TestDef { date: "9/21/1941", challenge: Some(1), gift: Some(8) },
            TestDef { date: "9/22/1941", challenge: Some(0), gift: None },
            TestDef { date: "9/23/1941", challenge: Some(1), gift: Some(8) },
            TestDef { date: "9/24/1941", challenge: Some(2), gift: Some(7) },
            TestDef { date: "9/25/1941", challenge: Some(3), gift: Some(6) },
            TestDef { date: "9/26/1941", challenge: Some(4), gift: Some(5) },
            TestDef { date: "9/27/1941", challenge: Some(5), gift: Some(4) },
            TestDef { date: "9/28/1941", challenge: Some(6), gift: Some(3) },
            TestDef { date: "9/29/1941", challenge: Some(7), gift: Some(2) },
            TestDef { date: "9/30/1941", challenge: Some(3), gift: Some(6) },
            TestDef { date: "5/31/1941", challenge: Some(2), gift: Some(7) },
        ];

        for test in table {
            let date = match Date::new(test.date) {
                Ok(rd) => rd,
                Err(e) => panic!("Invalid Date: {}", e.to_string()),
            };

            match date.challenges.birthday_challenge {
                Some(c1) => {
                    match test.challenge {
                        Some(c2) => assert_eq!(c1.to_challenge(), c2, "date: [{}] challenge", test.date),
                        None => panic!("Missing challenge for date: [{}]", test.date)
                    }
                }
                None => {
                    match test.challenge {
                        Some(_) => panic!("Unexpected challenge for date: [{}]", test.date),
                        None => assert!(true)
                    }
                }
            }

            match date.challenges.birthday_gift {
                Some(g1) => {
                    match test.gift {
                        Some(g2) => assert_eq!(g1.to_gift(), g2, "date: [{}] gift", test.date),
                        None => panic!("Missing gift for date: [{}]", test.date)
                    }
                }
                None => {
                    match test.gift {
                        Some(_) => panic!("Unexpected gift for date: [{}]", test.date),
                        None => assert!(true)
                    }
                }
            }
        }
    }

    #[test]
    fn test_major_minor_challenges() {
        struct TestDef<'a> {
            date: &'a str,
            minor1: u32,
            minor2: u32,
            major: u32,
            additional_minor: Option<u32>,
        }

        let table = vec![
            TestDef { date: "5/6/1959", minor1: 1, minor2: 0, major: 1, additional_minor: None },
            TestDef { date: "9/6/1941", minor1: 3, minor2: 0, major: 3, additional_minor: None },
            TestDef { date: "1/3/1908", minor1: 2, minor2: 6, major: 4, additional_minor: Some(8) },
        ];

        for test in table {
            let date = match Date::new(test.date) {
                Ok(rd) => rd,
                Err(e) => panic!("Invalid Date: {}", e.to_string()),
            };
            assert_eq!(date.challenges.minor1, test.minor1, "date: [{}] minor1", test.date);
            assert_eq!(date.challenges.minor2, test.minor2, "date: [{}] minor2", test.date);
            assert_eq!(date.challenges.major, test.major, "date: [{}] major", test.date);
            assert_eq!(date.challenges.additional_minor, test.additional_minor, "date: [{}] additional_minor", test.date);
        }
    }
}