#![warn(missing_docs)]

//! # date:pinnacles::attainment_cycles::types
//!
//! The date:pinnacles::attainment_cycles::types module implements names of the attainment cycles

use std::fmt;

use crate::date::basic_date::BasicDate;
use crate::error::NumerolibError;

/// Names enum payload
#[derive(Clone, Debug)]
pub struct Cycle {
    /// The cycle number
    pub num: u32,
    /// The start year
    pub start: u32,
}

impl Cycle {
    /// Create a new Cycle instance
    ///
    /// # Arguments
    /// * `cycle_number` - The number of the cycle (1-11)
    /// * `date` - The date used to calculate the starting year
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// use numerolib::date::pinnacles::attainment_cycles::types::Cycle;
    /// let date = BasicDate::new("9/22/1941").unwrap();
    /// let cycle1 = Cycle::new(1, &date).unwrap();
    /// assert_eq!(cycle1.num, 1);
    /// assert_eq!(cycle1.start, 1941);
    /// let cycle5 = Cycle::new(5, &date).unwrap();
    /// assert_eq!(cycle5.num, 5);
    /// assert_eq!(cycle5.start, 1977);
    /// ```
    pub fn new(cycle_number: u32, date: &BasicDate) -> Result<Self, NumerolibError> {
        if cycle_number < 1 || cycle_number > 11 {
            return Err(NumerolibError::OutOfRangeError(cycle_number as usize));
        }
        Ok(Self {
            num: cycle_number,
            start: date.year() + 9 * (cycle_number - 1),
        })
    }
}

/// Negative Path of Life Keywords
#[allow(missing_docs)]
#[derive(Clone, Debug)]
pub enum Types {
    Individualization(Cycle),
    Association(Cycle),
    SelfExpression(Cycle),
    Work(Cycle),
    Freedom(Cycle),
    Responsibility(Cycle),
    Introspection(Cycle),
    MaterialAspects(Cycle),
    Encompassing(Cycle),
    Rebirth(Cycle),
    Inspiration(Cycle),
}

impl Types {
    /// Get the enum for the given cycle number
    ///
    /// # Arguments
    ///
    /// * `cycle_number` - The cycle for which to create the name
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 8, THE ATTAINMENT CYCLES
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// use numerolib::date::pinnacles::attainment_cycles::types;
    /// use numerolib::date::pinnacles::attainment_cycles::types::Types;
    /// let date = BasicDate::new("9/22/1941").unwrap();
    /// let name = types::Types::new(4, &date).unwrap();
    /// match &name {
    ///     Types::Work(cycle) => {
    ///         assert_eq!(cycle.num, 4);
    ///         assert_eq!(cycle.start, 1968);
    ///     }
    ///     _ => panic!("Invalid name")
    /// }
    /// assert_eq!(&name.to_string(), "Work");
    /// ```
    ///
    pub fn new(cycle_number: u32, date: &BasicDate) -> Result<Self, NumerolibError> {
        let cycle = Cycle::new(cycle_number, date)?;
        Ok(match cycle_number {
            1 => Types::Individualization(cycle),
            2 => Types::Association(cycle),
            3 => Types::SelfExpression(cycle),
            4 => Types::Work(cycle),
            5 => Types::Freedom(cycle),
            6 => Types::Responsibility(cycle),
            7 => Types::Introspection(cycle),
            8 => Types::MaterialAspects(cycle),
            9 => Types::Encompassing(cycle),
            10 => Types::Rebirth(cycle),
            11 => Types::Inspiration(cycle),
            _ => return Err(NumerolibError::OutOfRangeError(cycle_number as usize)),
        })
    }

    /// Convert to cycle
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// use numerolib::date::pinnacles::attainment_cycles::types::Types;
    /// let date = BasicDate::new("9/22/1941").unwrap();
    /// let name = Types::new(4, &date).unwrap();
    /// let cycle = name.to_cycle();
    /// assert_eq!(cycle.num, 4);
    /// assert_eq!(cycle.start, 1968);
    /// ```
    ///
    pub fn to_cycle(&self) -> &Cycle {
        match self {
            Types::Individualization(cycle) |
            Types::Association(cycle) |
            Types::SelfExpression(cycle) |
            Types::Work(cycle) |
            Types::Freedom(cycle) |
            Types::Responsibility(cycle) |
            Types::Introspection(cycle) |
            Types::MaterialAspects(cycle) |
            Types::Encompassing(cycle) |
            Types::Rebirth(cycle) |
            Types::Inspiration(cycle) => cycle
        }
    }

    /// Convert to start year
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// use numerolib::date::pinnacles::attainment_cycles::types::Types;
    /// let date = BasicDate::new("9/22/1941").unwrap();
    /// let name = Types::new(4, &date).unwrap();
    /// let start_year = name.to_start_year();
    /// assert_eq!(start_year, 1968);
    /// ```
    ///
    pub fn to_start_year(&self) -> u32 {
        self.to_cycle().start
    }
}

impl fmt::Display for Types {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Types::Individualization(_) => write!(f, "Individualization"),
            Types::Association(_) => write!(f, "Association"),
            Types::SelfExpression(_) => write!(f, "Self-expression"),
            Types::Work(_) => write!(f, "Work"),
            Types::Freedom(_) => write!(f, "Freedom"),
            Types::Responsibility(_) => write!(f, "Responsibility"),
            Types::Introspection(_) => write!(f, "Introspection"),
            Types::MaterialAspects(_) => write!(f, "MaterialAspects"),
            Types::Encompassing(_) => write!(f, "Encompassing"),
            Types::Rebirth(_) => write!(f, "Rebirth"),
            Types::Inspiration(_) => write!(f, "Inspiration"),
        }
    }
}


