#![warn(missing_docs)]

//! # date:pinnacles::attainment_cycles
//!
//! The date:pinnacles::attainment_cycles module implements the Attainments if the Pinnacles

use crate::date::basic_date::BasicDate;
use crate::date::pinnacles::attainment_cycles::types::Types;
use crate::error::NumerolibError;

pub mod types;

/// The Cycles struct contains a list of attainment cycles
pub struct Cycles {
    cycles: Vec<types::Types>,
}

impl Cycles {
    /// Create a full set of 11 attainment cycles for the given birth date
    ///
    /// # Arguments
    /// `date` - The &BasicDate for the birth date
    ///
    /// # Examples
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 8, THE ATTAINMENT CYCLES
    ///
    /// ```
    /// use numerolib::date::basic_date::BasicDate;
    /// use numerolib::date::pinnacles::attainment_cycles::Cycles;
    /// let date = BasicDate::new("9/22/1941").unwrap();
    /// let cycles = Cycles::new(&date);
    /// assert_eq!(cycles.get(1).unwrap().to_cycle().num, 1);
    /// assert_eq!(cycles.get(5).unwrap().to_start_year(), 1977);
    /// assert_eq!(cycles.get(10).unwrap().to_string(), "Rebirth");
    /// ```
    ///
    pub fn new(date: &BasicDate) -> Self {
        let mut cycles = Vec::new();
        for i in 1..=11 {
            cycles.push(types::Types::new(i, &date).unwrap());
        }

        Self { cycles }
    }

    /// Get the specified cycle
    ///
    /// # Arguments
    ///
    /// * `index` - The cycle number, must be 1..=11
    pub fn get(&self, index: u32) -> Result<Types, NumerolibError> {
        let i = index as usize;
        if i < 1 || i > 11 { return Err(NumerolibError::OutOfRangeError(i)); }

        Ok(self.cycles[i - 1].clone())
    }
}
