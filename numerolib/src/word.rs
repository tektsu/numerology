#![warn(missing_docs)]

//! # word
//!
//! The word module performs calculations on a word or number string

use std::fmt;

use string_builder::Builder;

/// A Word holds all the reductions of a single word
#[derive(Debug)]
pub struct Word {
    /// The word being reduced
    pub word: String,
    /// The single-digit value of the word, even where there is a master number
    pub fully_reduced_result: u32,
    /// Any master number found during the reduction
    pub master: Option<u32>,
    /// Final result, single-digit or master number
    pub result: u32,
    /// Result as a string
    pub result_str: String,
    /// Word reduction as a string
    pub reduction_str: String,
}

impl Word {
    /// Create a new ['Word'] instance
    ///
    /// # Arguments
    ///
    /// `word_in` - The word to be reduced
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::word::Word;
    /// let word = Word::new("Steve");
    /// assert_eq!(word.word, "Steve");
    /// assert_eq!(word.result, 8);
    /// assert_eq!(word.result_str, "8");
    /// ```
    ///
    /// ```
    /// use numerolib::word::Word;
    /// let word = Word::new("Akira");
    /// assert_eq!(word.word, "Akira");
    /// assert_eq!(word.result, 22);
    /// assert_eq!(word.result_str, "22/4");
    /// ```
    ///
    /// From _The Numbers of Life: The Hidden Power of Numerology_, Ch 2, THE PATH OF LIFE
    ///
    /// ```
    /// use numerolib::word::Word;
    /// let word = Word::new("9/24/1941");
    /// assert_eq!(word.result, 3);
    /// assert_eq!(word.result_str, "3");
    /// ```
    ///
    /// ```
    /// use numerolib::word::Word;
    /// let word = Word::new("1/24/1941");
    /// assert_eq!(word.result, 22);
    /// assert_eq!(word.result_str, "22/4");
    /// ```
    ///
    pub fn new<S: Into<String>>(word_in: S) -> Word {
        let word = word_in.into();
        let mut word_to_reduce = word.clone();
        let mut reductions = Vec::new();
        let mut fully_reduced_result: u32;

        // Check for case that the word is a master number
        let mut master = match word.trim() {
            "11" => Some(11),
            "22" => Some(22),
            _ => None
        };

        // Cycle through the reductions until there is a single digit result
        loop {
            let mut reduction = Vec::new();
            fully_reduced_result = 0;
            for ch in word_to_reduce.chars() {
                let val = Self::letter_value(ch);
                reduction.push(val);
                fully_reduced_result += val;
            }
            reductions.push(reduction);

            if fully_reduced_result == 11 || fully_reduced_result == 22 { master = Some(fully_reduced_result); }

            if fully_reduced_result <= 9 { break; }

            word_to_reduce = fully_reduced_result.to_string();
        }

        // Build result string
        let mut builder = Builder::default();
        match master {
            Some(m) => builder.append(format!("{}/", m)),
            None => {}
        }
        builder.append(format!("{}", fully_reduced_result));
        let result_str = builder.string().unwrap();

        // Build reduction string
        let mut builder = Builder::default();
        if reductions.len() > 1 {
            for reduction in &reductions {
                for v in reduction {
                    builder.append(format!("{}", v));
                }
                builder.append(" = ");
            }
        }
        builder.append(format!("{}", result_str));
        let reduction_str = builder.string().unwrap();

        // Calculate final result
        let result = match master {
            Some(m) => m,
            None => fully_reduced_result
        };

        Word { word, fully_reduced_result, master, result, result_str, reduction_str }
    }

    /// Create a new ['Word'] instance from a u32
    ///
    /// # Arguments
    ///
    /// `value` - The number to be reduced
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::word::Word;
    /// let word = Word::new_from_u32(1977);
    /// assert_eq!(word.word, "1977");
    /// assert_eq!(word.result, 6);
    /// assert_eq!(word.result_str, "6");
    /// ```
    ///
    pub fn new_from_u32(value: u32) -> Word {
        Self::new(format!("{}", value))
    }

    /// Get a string representation of this Word
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::word::Word;
    /// let word = Word::new("Janet");
    /// assert_eq!(word.to_string(), "Janet: 11552 = 14 = 5");
    /// ```
    ///
    pub fn to_string(&self) -> String {
        format!("{}: {}", self.word, self.reduction_str)
    }

    fn letter_value(c: char) -> u32 {
        match c {
            '1' | 'a' | 'j' | 's' | 'A' | 'J' | 'S' => 1,
            '2' | 'b' | 'k' | 't' | 'B' | 'K' | 'T' => 2,
            '3' | 'c' | 'l' | 'u' | 'C' | 'L' | 'U' => 3,
            '4' | 'd' | 'm' | 'v' | 'D' | 'M' | 'V' => 4,
            '5' | 'e' | 'n' | 'w' | 'E' | 'N' | 'W' => 5,
            '6' | 'f' | 'o' | 'x' | 'F' | 'O' | 'X' => 6,
            '7' | 'g' | 'p' | 'y' | 'G' | 'P' | 'Y' => 7,
            '8' | 'h' | 'q' | 'z' | 'H' | 'Q' | 'Z' => 8,
            '9' | 'i' | 'r' | 'I' | 'R' => 9,
            _ => 0,
        }
    }
}

impl fmt::Display for Word {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

//==================================================================================================
// Unit Tests
//==================================================================================================

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_word() {
        struct TestDef<'a> {
            word: &'a str,
            result_str: &'a str,
            result: u32,
        }

        let table = vec![
            TestDef { word: "Stephen", result_str: "6", result: 6 },
            TestDef { word: "Janet Audrey Hendrich", result_str: "4", result: 4 },
            TestDef { word: "Janet-Audrey /Hendrich", result_str: "4", result: 4 },
            TestDef { word: "1941", result_str: "6", result: 6 },
            TestDef { word: "9/24/1941", result_str: "3", result: 3 },
            TestDef { word: "1/24/1941", result_str: "22/4", result: 22 },
            TestDef { word: "1941 9 24", result_str: "3", result: 3 },
            TestDef { word: "---", result_str: "0", result: 0 },
            TestDef { word: "22", result_str: "22/4", result: 22 },
            TestDef { word: " 22", result_str: "22/4", result: 22 },
            TestDef { word: "22 ", result_str: "22/4", result: 22 },
            TestDef { word: "  11  ", result_str: "11/2", result: 11 },
        ];

        for test in table {
            let run_tests = |word: Word, note: &str| {
                assert_eq!(word.word, test.word, "word: [{}] note: [{}]", test.word, note);
                assert_eq!(word.result_str, test.result_str, "word: [{}] note: [{}]", test.word, note);
                assert_eq!(word.result, test.result, "word: [{}] note: [{}]", test.word, note);
            };
            run_tests(Word::new(test.word), "as str");
            run_tests(Word::new(test.word.to_string()), "as string");
        }
    }

    #[test]
    fn test_new_word_from_u32() {
        struct TestDef<'a> {
            value: u32,
            result_str: &'a str,
            result: u32,
        }

        let table = vec![
            TestDef { value: 1941, result_str: "6", result: 6 },
        ];

        for test in table {
            let word = Word::new_from_u32(test.value);
            assert_eq!(word.result_str, test.result_str, "value: [{}]", test.value);
            assert_eq!(word.result, test.result, "value: [{}]", test.value);
        }
    }

    #[test]
    fn test_master_result() {
        struct TestDef<'a> {
            word: &'a str,
            master_result: u32,
        }

        let table = vec![
            TestDef { word: "9/24/1941", master_result: 3 },
            TestDef { word: "1/24/1941", master_result: 22 },
        ];

        for test in table {
            let word = Word::new(test.word);
            assert_eq!(word.result, test.master_result, "word: [{}]", test.word);
        }
    }

    #[test]
    fn test_reduction_string() {
        struct TestDef<'a> {
            word: &'a str,
            reduction: &'a str,
        }

        let table = vec![
            TestDef { word: "Akira", reduction: "12991 = 22 = 22/4" },
            TestDef { word: "Steve", reduction: "12545 = 17 = 8" },
            TestDef { word: "1942", reduction: "1942 = 16 = 7" },
        ];

        for test in table {
            let word = Word::new(test.word);
            assert_eq!(word.reduction_str, test.reduction, "word: [{}]", test.word);
        }
    }
}