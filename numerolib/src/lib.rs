#![warn(missing_docs)]

//! # numerology
//!
//! The numerology module provides structures and operations to implement a numerology chart

/// Implement the word module
pub mod word;

/// Implement the name module
pub mod name;

/// Implement the date module
pub mod date;

/// Custom error type
pub mod error;
