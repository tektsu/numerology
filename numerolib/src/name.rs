#![warn(missing_docs)]

//! # name
//!
//! The name module preforms calculations on a full name

use std::fmt;

use string_builder::Builder;

use crate::error::NumerolibError;
use crate::word::Word;

/// A Name holds all the reductions of a full name
pub struct Name {
    /// A list of reductions of each part of the name, starting with the full name
    names: Vec<Word>,
}

impl Name {
    /// Create a new ['Name'] instance
    ///
    /// # Arguments
    ///
    /// `name_in` - The name to be reduced
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::name::Name;
    /// let name = Name::new("Janet Audrey Hendrich");
    /// for i in 0..4 {     ///
    ///     let word = name.name_from_index(i).unwrap();
    ///     match i {
    ///         0 => assert_eq!(word.word, "Janet Audrey Hendrich"),
    ///         1 => assert_eq!(word.word, "Janet"),
    ///         2 => assert_eq!(word.word, "Audrey"),
    ///         3 => assert_eq!(word.word, "Hendrich"),
    ///         _ => panic!("{} is not a valid index", i),
    ///     }
    /// }
    /// ```
    ///
    pub fn new<S: Into<String>>(name_in: S) -> Self {
        let name = name_in.into();
        let mut names = Vec::new();
        names.push(Word::new(name.clone()));
        for word in name.clone().split_whitespace() {
            names.push(Word::new(word.to_string()));
        }

        Self { names }
    }

    /// Get a WordData struct for part of the name (index 0 is the full name)
    ///
    /// # Arguments
    ///
    /// `index` - The index of the WordData to return
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::name::Name;
    /// let name = Name::new("Janet Audrey Hendrich");
    /// let data = name.name_from_index(1).unwrap();
    /// assert_eq!(data.word, "Janet");
    /// ```
    ///
    pub fn name_from_index(&self, index: usize) -> Result<&Word, NumerolibError> {
        if index >= self.names.len() {
            return Err(NumerolibError::OutOfRangeError(index));
        }

        Ok(&self.names[index])
    }

    /// Get a string representation of this Name
    ///
    /// # Examples
    ///
    /// ```
    /// use numerolib::name::Name;
    /// let name = Name::new("Janet Audrey Hendrich").to_string();
    /// ```
    ///
    pub fn to_string(&self) -> String {
        let mut builder = Builder::default();
        for name in &self.names {
            builder.append(format!("{}\n", name.to_string()));
        }

        builder.string().unwrap().trim().to_string()
    }
}

impl fmt::Display for Name {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_name() {
        struct WordDef<'a> {
            word: &'a str,
            result_str: &'a str,
            result: u32,
        }

        struct TestDef<'a> {
            name: &'a str,
            names: Vec<WordDef<'a>>,
        }

        let table = vec![
            TestDef {
                name: "Janet Audrey Hendrich",
                names: vec![
                    WordDef { word: "Janet Audrey Hendrich", result_str: "4", result: 4 },
                    WordDef { word: "Janet", result_str: "5", result: 5 },
                    WordDef { word: "Audrey", result_str: "11/2", result: 11 },
                    WordDef { word: "Hendrich", result_str: "6", result: 6 },
                ],
            },
        ];

        for test in table {
            let run_tests = |name: Name, note: &str| {
                // assert_eq!(name.name, test.name, "name: [{}] note: [{}]", test.name, note);
                let names_length = test.names.len();
                // assert_eq!(name.names.len(), names_length, "name: [{}] note: [{}]", test.name, note);
                for i in 0..names_length {
                    let data = name.name_from_index(i).unwrap();
                    assert_eq!(data.word, test.names[i].word, "name: [{}] index: [{}] note: [{}]", test.name, i, note);
                    assert_eq!(data.result_str, test.names[i].result_str, "name: [{}] index: [{}] note: [{}]", test.name, i, note);
                    assert_eq!(data.result, test.names[i].result, "name: [{}] index: [{}] note: [{}]", test.name, i, note);
                }
            };
            run_tests(Name::new(test.name), "as str");
            run_tests(Name::new(test.name.to_string()), "as string");
        }
    }

    #[test]
    fn test_name_from_index() {
        let name = Name::new("Janet Audrey Hendrich");
        match name.name_from_index(4) {
            Ok(_) => { panic!("Index 4 should have errored") }
            Err(e) => {
                match e {
                    NumerolibError::OutOfRangeError(i) => {
                        assert_eq!(i, 4);
                    }
                    _ => { panic!("Should have returned a NumerolibError::OutOfRangeError()"); }
                }
            }
        }
    }
}

