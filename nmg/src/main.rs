// use numerology;
use numerolib::date::Date;
use numerolib::name::Name;

fn main() {
    let reduced_name = Name::new("Janet Audrey Hendrich");
    println!("{}", reduced_name);

    let date = match Date::new("9/24/1941") {
        Ok(rd) => rd,
        Err(e) => panic!("Date Error {}", e),
    };
    println!("{}", date);
    //
    // println!("\nSYNOPSIS");
    //
    // println!("  Path of Life: {}", date.path_of_life.reduction_as_string());
    //
    // println!("  Formative Cycle  begins in {} at birth:  {} ({})", date.formative.beginning_year, date.formative.cycle.result, positive_keyword(date.formative.cycle.result).unwrap());
    // let pcycle = date.productive_cycle();
    // let pcycle_begins = date.year_productive_cycle_begins();
    // let age = date.age_in(pcycle_begins).unwrap();
    // println!("  Productive Cycle begins in {} at age {}: {} ({})", pcycle_begins, age, pcycle, positive_keyword(pcycle).unwrap());
    // let hcycle = date.harvest_cycle();
    // let hcycle_begins = date.year_harvest_cycle_begins();
    // let age = date.age_in(hcycle_begins).unwrap();
    // print!("  Harvest Cycle    begins in {} at age {}: {} ({})", hcycle_begins, age, hcycle, positive_keyword(hcycle).unwrap());
    // println!("");
}